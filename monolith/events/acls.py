from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json


def get_photo(city):
    response = requests.get(
        url=f"https://api.pexels.com/v1/search?query={city}",
        headers={"Authorization": PEXELS_API_KEY},
    )
    return response.json()["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    geo_params = {
        "q": f"{city}, {state}, US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }
    geo_url = "http://api.openweathermap.org/geo/1.0/direct?"
    response = requests.get(geo_url, params=geo_params)
    content = json.loads(response.content)
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None
    weather_url = "https://api.openweathermap.org/data/2.5/weather"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "appid": OPEN_WEATHER_API_KEY,
        "units": "metric",
    }
    response = requests.get(weather_url, params=weather_params)
    content = json.loads(response.content)
    try:
        return {
            "description": content["weather"][0]["description"],
            "temp": content["main"]["temp"],
        }
    except (KeyError, IndexError):
        return "This is the problem, check Index error or Key error"
